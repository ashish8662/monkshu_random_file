/* 
 * (C) 2015 TekMonks. All rights reserved.
 * License: GPL2 - see enclosed LICENSE file.
 */

exports.doService = async jsonReq => {
    try {
        return { "result": true, "data": Math.sqrt(Math.random()*100) };
    } catch (err) { LOG.error(err); return CONSTANTS.FALSE_RESULT; }
}