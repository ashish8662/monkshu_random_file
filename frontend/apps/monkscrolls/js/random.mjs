import { xhr } from "/framework/js/xhr.mjs";

const generateNumber = async () => {
    const results = await xhr.rest(APP_CONSTANTS.API_RANDOM, "POST");
    if (results.result) document.getElementById('randomNumber').innerHTML = results.data;
}

export const random = { generateNumber }

