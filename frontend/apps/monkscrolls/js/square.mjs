import { xhr } from "/framework/js/xhr.mjs";
import {router} from "/framework/js/router.mjs";

const generateNumber = async () => {
    const results = await xhr.rest(APP_CONSTANTS.API_SQUARE, "POST");
    if (results.result) document.getElementById('squareNumber').innerHTML = results.data;
}

export const square = { generateNumber }